/*-
 * session.c
 *
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2020 Marc Veldman <marc@bumblingdork.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 * $FreeBSD$
 */
#include <sys/ioctl.h>
#define L2CAP_SOCKET_CHECKED
#include <bluetooth.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <netgraph/bluetooth/include/ng_l2cap.h>

#include "libatt-test.h"

int
att_open(bdaddr_t const *l, bdaddr_t const *r, uint8_t addrtype)
{
	int			s;
	struct sockaddr_l2cap	sa;
	socklen_t		size;

	if (l == NULL || r == NULL) {
		goto fail;
	}

	s = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BLUETOOTH_PROTO_L2CAP);
	if (s < 0) {
		goto fail;
	}

	sa.l2cap_cid = 0;
	sa.l2cap_len = sizeof(sa);
	sa.l2cap_family = AF_BLUETOOTH;
	sa.l2cap_psm = 0;
	sa.l2cap_bdaddr_type = BDADDR_LE_PUBLIC;

	
	memcpy(&sa.l2cap_bdaddr, l, sizeof(sa.l2cap_bdaddr));
	if (bind(s, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		goto fail;
	}

	/* Destination socket */
	sa.l2cap_cid = 0x04;
	if (0 == addrtype) {
		printf("PUBLIC \n");
		sa.l2cap_bdaddr_type = BDADDR_LE_PUBLIC;
	} else {
		printf("RANDOM \n");
		sa.l2cap_bdaddr_type = BDADDR_LE_RANDOM;
	}
	memcpy(&sa.l2cap_bdaddr, r, sizeof(sa.l2cap_bdaddr));
	if (connect(s, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		goto fail;
	}

	return s;
fail:
	printf("Error: %d\n", errno);
	return (-1);
}

void
att_close(int s)
{
	close(s);
}
