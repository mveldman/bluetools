#define L2CAP_SOCKET_CHECKED 
#include <stdlib.h>
#include <err.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <att.h>
#include <bluetooth.h>

#include "libatt-test.h"

int
main(int argc, char *argv[])
{
	bdaddr_t		 src, dst;
	struct hostent		*he;
	int32_t			 n;
	int8_t			 addrtype = 0;
	int 			 s;
	int16_t			 mtu;
	uint16_t		 start_handle, end_handle, type, len;
	int		 	 res;
	att_attribute_type_p	 attribute_type;

	att_attribute_data_list_p 	 data_list;
	

	/* Set defaults */
	memcpy(&src, NG_HCI_BDADDR_ANY, sizeof(src));
	memcpy(&dst, NG_HCI_BDADDR_ANY, sizeof(dst));

	/* Parse command line arguments */
	while ((n = getopt(argc, argv, "s:a:t:")) != -1) {
		switch (n) {
		case 'a':
			if (!bt_aton(optarg, &dst)) {
				if ((he = bt_gethostbyname(optarg)) == NULL)
					errx(1, "%s: %s", optarg, hstrerror(h_errno));

				memcpy(&dst, he->h_addr, sizeof(dst));
			}
			break;

		case 's':
			if (!bt_aton(optarg, &src)) {
				if ((he = bt_gethostbyname(optarg)) == NULL)
					errx(1, "%s: %s", optarg, hstrerror(h_errno));

				memcpy(&src, he->h_addr, sizeof(src));
			}
			break;
		case 't':
			if (strcmp(optarg, "public") == 0) {
				addrtype = 0;
			} else {
				addrtype = 1;
			}
		}
	}
	
	s = att_open(&src, &dst, addrtype);
	mtu = att_mtu_exchange(s, 0x1000);
	printf("MTU: %04x\n", mtu);

	start_handle = 0x0001;
	end_handle = 0xffff;
	type = htole16(0x2800);
	attribute_type = malloc(sizeof(att_attribute_type) + sizeof(type));
	memset(attribute_type, 3, (sizeof(att_attribute_type) + sizeof(type)));
	attribute_type->length = sizeof(type);
	memcpy(attribute_type->value, &type, sizeof(type));
	data_list = att_read_by_group_type(s, start_handle, end_handle, 
		attribute_type);  
	printf("data list length: %02hd\n", data_list->list_length);
	printf("data data length: %02hd\n", data_list->data_length);
	att_free_data_list(data_list);
	att_find_information(s, 0x001f, 0x0030);
	
	att_data_p	att_data;
	printf("Voor data create\n");
        uint16_t t = 0x2800;
	att_data = att_data_create((void *) &t, sizeof(uint16_t)); 
	printf("voor find_by_type_value\n");
	att_find_by_type_value(s, 0x001f, 0x0030, 0x2800, att_data);
	close(s);
	
	return (0);
} /* main */
