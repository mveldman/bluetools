#define L2CAP_SOCKET_CHECKED 
#include <bluetooth.h>
#include <err.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "gatt.h"
#include "gatt-int.h"

int
main(int argc, char *argv[])
{
	bdaddr_t		 src, dst;
	struct hostent		*he;
	int32_t			 n;
	int8_t			addrtype = 0;
	void			*sess;

	/* Set defaults */
	memcpy(&src, NG_HCI_BDADDR_ANY, sizeof(src));
	memcpy(&dst, NG_HCI_BDADDR_ANY, sizeof(dst));

	/* Parse command line arguments */
	while ((n = getopt(argc, argv, "s:a:t:")) != -1) {
		switch (n) {
		case 'a':
			if (!bt_aton(optarg, &dst)) {
				if ((he = bt_gethostbyname(optarg)) == NULL)
					errx(1, "%s: %s", optarg, hstrerror(h_errno));

				memcpy(&dst, he->h_addr, sizeof(dst));
			}
			break;

		case 's':
			if (!bt_aton(optarg, &src)) {
				if ((he = bt_gethostbyname(optarg)) == NULL)
					errx(1, "%s: %s", optarg, hstrerror(h_errno));

				memcpy(&src, he->h_addr, sizeof(src));
			}
			break;
		case 't':
			if (strcmp(optarg, "public") == 0) {
				addrtype = 0;
			} else {
				addrtype = 1;
			}
		}
	}
	
	sess = gatt_open(&src, &dst, addrtype);
	printf("Exchange MTU\n");
	gatt_mtu_exchange((gatt_session_p) sess);
	/* Unknown */
	/* 
	printf("-------Start Unknown------\n");
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x0001, 0xffff);
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x000a, 0xffff);
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x0028, 0xffff);
	printf("-------End Unknown--------\n");
	for (uint8_t i = 1; i <= 5; i++) {
		printf("Read value of handle %04x\n", i);
		gatt_read_attribute((gatt_session_p) sess, i);
	}
	for (uint8_t i = 6; i <= 9; i++) {
		printf("Read value of handle %04x\n", i);
		gatt_read_attribute((gatt_session_p) sess, i);
	}
	for (uint8_t i = 10; i <= 15; i++) {
		printf("Read value of handle %04x\n", i);
		gatt_read_attribute((gatt_session_p) sess, i);
	}

	*/
	/* Polar */
	/*
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x0001, 0xffff);
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x0015, 0xffff);
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x0027, 0xffff);
	printf("Read handles 0x0024 to 0x0026 (Battery service handles)\n");
	gatt_find_information((gatt_session_p) sess, 0x0024, 0x0026);
	printf("--------------------------\n");
	printf("Read value of handle 0x0024\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0024);
	printf("Read value of handle 0x0025\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0025);
	printf("Read value of handle 0x0026\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0026);
	printf("Read value of handle 0x0027\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0027);
	printf("--------------------------\n");
	printf("Read handles 0x000f to 0x0014 (Heart rate service service handles)\n");
	gatt_find_information((gatt_session_p) sess, 0x000f, 0x0014);
	printf("--------------------------\n");
	printf("Read value of handle 0x000f\n");
	gatt_read_attribute((gatt_session_p) sess, 0x000f);
	printf("Read value of handle 0x0010\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0010);
	printf("Read value of handle 0x0011\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0011);
	printf("Read value of handle 0x0012\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0012);
	printf("Read value of handle 0x0013\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0013);
	printf("Read value of handle 0x0014\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0014);
	printf("--------------------------\n");
	printf("---Writing CCCD 0x012-----\n");
	gatt_write_attribute((gatt_session_p) sess, 0x0012, 0x0001);
	printf("Reading...\n");
	for (int i =0; i < 10; i++) {
		gatt_listen((gatt_session_p) sess);
	}
	*/

	/* Thingy */
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x000e, 0xffff);
	gatt_read_by_group_type((gatt_session_p) sess, 0x2800, 0x0062, 0xffff);
	printf("Read handles 0x001f to 0x0030 (Env service handles)\n");
	printf("---------------Start Find Information--------------\n");
	gatt_find_information((gatt_session_p) sess, 0x001f, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x0021, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x0022, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x0024, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x0025, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x0027, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x0028, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x002a, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x002b, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x002d, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x002e, 0x0030);
	gatt_find_information((gatt_session_p) sess, 0x0030, 0x0030);
	printf("---------------End Find Information----------------\n");
	printf("--------------------------\n");
	printf("Read value of handle 0x001f\n");
	gatt_read_attribute((gatt_session_p) sess, 0x001f);
	printf("Read value of handle 0x0020\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0020);
	printf("Read value of handle 0x0021\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0021);
	printf("Read value of handle 0x0022\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0022);
	printf("Read value of handle 0x0023\n");
	gatt_read_attribute((gatt_session_p) sess, 0x0023);
	printf("--------------------------\n");
	/*
	gatt_write_attribute((gatt_session_p) sess, 0x0022, 0x0001);
	gatt_write_attribute((gatt_session_p) sess, 0x0025, 0x0001);
	gatt_write_attribute((gatt_session_p) sess, 0x0028, 0x0001);
	gatt_write_attribute((gatt_session_p) sess, 0x002b, 0x0001);
	*/
	/*
	printf("Reading...\n");
	for (int i =0; i < 10; i++) {
		sess = gatt_open(&src, &dst, addrtype);
		gatt_listen((gatt_session_p) sess);
		gatt_close(sess);
	}
	*/
	gatt_close(sess);
	return (0);
} /* main */
