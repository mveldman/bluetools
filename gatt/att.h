/*-
 * att.h
 *
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2020 Marc Veldman <marc@bumblingdork.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 * $FreeBSD$
 */
#ifndef BLUETOOLS_ATT_H
#define BLUETOOLS_ATT_H

#define ATT_ERROR_RSP          		0x01
typedef struct {
        u_int8_t      	opcode;
        u_int8_t      	err_opcode;
        u_int16_t       err_handle;
        u_int16_t       err_code;
}__attribute__((packed)) att_error_mtu_rsp_pdu;

#define ATT_EXCHANGE_MTU_REQ    	0x02
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       client_rx_mtu;
}__attribute__((packed)) att_exchange_mtu_req_pdu;

#define ATT_EXCHANGE_MTU_RSP    	0x03
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       server_rx_mtu;
}__attribute__((packed)) att_exchange_mtu_rsp_pdu;

#define ATT_FIND_INFORMATION_REQ	0x04
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       start;
        u_int16_t       end;
}__attribute__((packed)) att_find_information_req_pdu;

#define ATT_FIND_INFORMATION_RSP	0x05
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       format;
        u_int16_t       data;
}__attribute__((packed)) att_find_information_rsp_pdu;

#define ATT_FIND_BY_TYPE_VALUE_REQ	0x06
#define ATT_FIND_BY_TYPE_VALUE_RSP	0x07

#define ATT_READ_BY_TYPE_REQ		0x08
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       start;
        u_int16_t       end;
        u_int16_t       type;
}__attribute__((packed)) att_read_by_type_req_pdu;

#define ATT_READ_BY_TYPE_RSP		0x09

#define ATT_READ_REQ			0x0a
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       handle;
}__attribute__((packed)) att_read_req_pdu;

#define ATT_READ_RSP			0x0b

#define ATT_READ_BLOB_REQ		0x0c
#define ATT_READ_BLOB_RSP		0x0d

#define ATT_READ_MULTIPLE_REQ		0x0e
#define ATT_READ_MULTIPLE_RSP		0x0f

#define ATT_READ_BY_GROUP_TYPE_REQ	0x10
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       start;
        u_int16_t       end;
        u_int16_t       group_type;
}__attribute__((packed)) att_read_by_group_type_req_pdu;

#define ATT_READ_BY_GROUP_TYPE_RSP	0x11

#define ATT_READ_MULTIPLE_VARIABLE_REQ	0x20
#define ATT_READ_MULTIPLE_VARIABLE_RSP	0x21

#define ATT_WRITE_REQ			0x12
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       handle;
        u_int16_t       value;
}__attribute__((packed)) att_write_req_pdu;

#define ATT_WRITE_RSP			0x13

#define ATT_WRITE_CMD			0x52

#define ATT_SIGNED_WRITE_CMD		0xd2

#define ATT_PREPARE_WRITE_REQ		0x16
#define ATT_PREPARE_WRITE_RSP		0x17

#define ATT_EXECUTE_WRITE_REQ		0x18
typedef struct {
        u_int8_t      	opcode;
        u_int16_t       flags;
}__attribute__((packed)) att_execute_write_req_pdu;
#define ATT_EXECUTE_WRITE_RSP		0x19

#define ATT_HANDLE_VALUE_NTF		0x1b
#define ATT_HANDLE_VALUE_IND		0x1d
#define ATT_HANDLE_VALUE_CFM		0x1e

#define ATT_MULTIPLE_HANDLE_VALUE_NTF	0x23

uint16_t att_find_information_req(const uint16_t sh, uint16_t eh, uint8_t *pdu);
uint16_t att_read_by_type_req(const uint16_t sh, uint16_t eh, uint16_t type, uint8_t *pdu);
uint16_t att_read_req(const uint16_t h, uint8_t *pdu);

#endif /* BLUETOOLS_ATT_H */
