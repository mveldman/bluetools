/*-
 * session.c
 *
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2020 Marc Veldman <marc@bumblingdork.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND 
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id$
 * $FreeBSD$
 */
#include <sys/ioctl.h>
#define L2CAP_SOCKET_CHECKED
#include <bluetooth.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <netgraph/bluetooth/include/ng_l2cap.h>

#include "gatt-int.h"
#include "gatt.h"
#include "att.h"

void *
gatt_open(bdaddr_t const *l, bdaddr_t const *r, uint8_t addrtype)
{
	gatt_session_p		gs = NULL;
	struct sockaddr_l2cap	sa;
	socklen_t		size;

	if ((gs = calloc(1, sizeof(*gs))) == NULL)
		goto fail;

	if (l == NULL || r == NULL) {
		gs->error = EINVAL;
		goto fail;
	}

	gs->s = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BLUETOOTH_PROTO_L2CAP);
	if (gs->s < 0) {
		gs->error = errno;
		goto fail;
	}

	sa.l2cap_cid = 0;
	sa.l2cap_len = sizeof(sa);
	sa.l2cap_family = AF_BLUETOOTH;
	sa.l2cap_psm = 0;
	sa.l2cap_bdaddr_type = BDADDR_LE_PUBLIC;

	
	memcpy(&sa.l2cap_bdaddr, l, sizeof(sa.l2cap_bdaddr));
	if (bind(gs->s, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		gs->error = errno;
		goto fail;
	}

	/* Destination socket */
	sa.l2cap_cid = 0x04;
	/* ATT */
	//sa.l2cap_psm = 0x001f;
	/* EATT */
	//sa.l2cap_psm = 0x0027;
	/* NO PSM */
	//sa.l2cap_psm = 0;
	if (0 == addrtype) {
		printf("PUBLIC \n");
		sa.l2cap_bdaddr_type = BDADDR_LE_PUBLIC;
	} else {
		printf("RANDOM \n");
		sa.l2cap_bdaddr_type = BDADDR_LE_RANDOM;
	}
	memcpy(&sa.l2cap_bdaddr, r, sizeof(sa.l2cap_bdaddr));
	if (connect(gs->s, (struct sockaddr *) &sa, sizeof(sa)) < 0) {
		gs->error = errno;
		goto fail;
	}

	size = sizeof(gs->omtu);
	if (getsockopt(gs->s, SOL_L2CAP, SO_L2CAP_OMTU, &gs->omtu, &size) < 0) {
		gs->error = errno;
		goto fail;
	}
	if ((gs->req = malloc(gs->omtu)) == NULL) {
		gs->error = ENOMEM;
		goto fail;
	}
	gs->req_e = gs->req + gs->omtu;

	size = sizeof(gs->imtu);
	if (getsockopt(gs->s, SOL_L2CAP, SO_L2CAP_IMTU, &gs->imtu, &size) < 0) {
		gs->error = errno;
		goto fail;
	}
	if ((gs->rsp = malloc(gs->imtu)) == NULL) {
		gs->error = ENOMEM;
		goto fail;
	}
	gs->rsp_e = gs->rsp + gs->imtu;
	gs->error = 0;
fail:
	if (gs->error != 0)
		printf("GS Error: %d\n", gs->error);
	return ((void *) gs);
}

int32_t
gatt_close(void *xgs)
{
	gatt_session_p	gs = (gatt_session_p) xgs;

	if (gs != NULL) {
		if (gs->s >= 0)
			close(gs->s);

		if (gs->req != NULL)
			free(gs->req);
		if (gs->rsp != NULL)
			free(gs->rsp);

		memset(gs, 0, sizeof(*gs));
		free(gs);
	}

	return (0);
}

int32_t
gatt_error(void *xgs)
{
	gatt_session_p	gs = (gatt_session_p) xgs;

	return ((gs != NULL)? gs->error : EINVAL);
}

int32_t
gatt_mtu_exchange(gatt_session_p gs)
{
	uint16_t len, mtu;
	uint8_t buf[2048];
	ssize_t res;
	att_exchange_mtu_req_pdu	pdu;

	pdu.opcode = ATT_EXCHANGE_MTU_REQ; 
	pdu.client_rx_mtu = 0xffff;
	write(gs->s, &pdu, sizeof(pdu));

   	memset(buf, 0, sizeof(buf));
   	res = read(gs->s, buf, sizeof(buf));
    	if (res > 0)
		gatt_print_response(res, buf);
	else
		printf("Gatt MTU Exchange failed: %d\n", errno);
   	return 0;
}

int32_t
gatt_find_information(gatt_session_p gs, uint16_t start, uint16_t end)
{
    	uint16_t len;
    	uint8_t buf[2048];
    	ssize_t res;
	att_find_information_req_pdu	pdu;
	
	pdu.opcode = ATT_FIND_INFORMATION_REQ;
	pdu.start = start;
	pdu.end = end;
	write(gs->s, &pdu, sizeof(pdu));

    	memset(buf, 0, sizeof(buf));
    	res = read(gs->s, buf, sizeof(buf));
        if (res > 0) {
		gatt_print_response(res, buf);
	}
	else {
		printf("Error: %d, res: %d\n", errno, (int)res);
	}
   	return 0;
}

int32_t
gatt_read_by_type(gatt_session_p gs, uint16_t type, uint16_t start, uint16_t end)
{
    	uint16_t len;
    	uint8_t buf[2048];
        att_read_by_type_req_pdu	pdu;
    	ssize_t res;
    	memset(buf, 0, sizeof(buf));

	pdu.opcode = ATT_READ_BY_TYPE_REQ;
	pdu.start = start;
	pdu.end = end;
	pdu.type = type;
  	res =write(gs->s, &pdu, sizeof(pdu));
 	printf("Written read_by_type: %d \n", (int)res);	
    	memset(buf, 0, sizeof(buf));
    	res = read(gs->s, buf, sizeof(buf));
        if (res > 0) 
		gatt_print_response(res, buf);
	else
		printf("Error: %d, res: %d\n", errno, (int) res);

   	return 0;
}

int32_t
gatt_read_by_group_type(gatt_session_p gs, uint16_t group_type, uint16_t start, uint16_t end)
{
    	uint16_t len;
    	uint8_t buf[2048];
        att_read_by_group_type_req_pdu	pdu;
    	ssize_t res;
    	memset(buf, 0, sizeof(buf));

	pdu.opcode = ATT_READ_BY_GROUP_TYPE_REQ;
	pdu.start = start;
	pdu.end = end;
	pdu.group_type = group_type;
	gatt_print_response(sizeof(pdu), (uint8_t *)&pdu);
  	res =write(gs->s, &pdu, sizeof(pdu));
 	printf("Written read_by_group_type: %d \n", (int)res);	
    	memset(buf, 0, sizeof(buf));
    	res = read(gs->s, buf, sizeof(buf));
        if (res > 0) 
		gatt_print_response(res, buf);
	else
		printf("Error: %d, res: %d\n", errno, (int) res);

   	return 0;
}

int32_t
gatt_read_attribute(gatt_session_p gs, uint16_t h)
{
    	uint16_t len;
    	uint8_t buf[2048];
    	ssize_t res;
	att_read_req_pdu	pdu;

	pdu.opcode = ATT_READ_REQ;
	pdu.handle = h;

	write(gs->s, &pdu, sizeof(pdu));

    	memset(buf, 0, sizeof(buf));
    	res = read(gs->s, buf, sizeof(buf));
        if (res > 0)
		gatt_print_response(res, buf);
	else
		printf("Error: %d\n", errno);

   	return 0;
}

int32_t
gatt_write_attribute(gatt_session_p gs, uint16_t h, uint16_t v)
{
	att_write_req_pdu	pdu;
    	uint8_t buf[2048];
    	ssize_t res;

	pdu.opcode = ATT_WRITE_REQ;
	pdu.handle = h;
	pdu.value = v;

	write(gs->s, &pdu, sizeof(pdu));

    	memset(buf, 0, sizeof(buf));
    	res = read(gs->s, buf, sizeof(buf));
        if (res > 0)
		gatt_print_response(res, buf);
	else
		printf("Error: %d\n", errno);


	return 0;
}

int32_t
gatt_listen(gatt_session_p gs)
{
    	uint8_t buf[2048];
    	ssize_t res;

    	memset(buf, 0, sizeof(buf));
    	res = read(gs->s, buf, sizeof(buf));
        if (res > 0)
		gatt_print_response(res, buf);
	else
		printf("Error: %d\n", errno);

	return 0;
}

void
gatt_print_response(uint16_t len, uint8_t *response)
{
	printf("Bytes read %02d\n", len);
	if (len > 0) {
		for (int i = 0; i < len; i++) {
			printf("%02x ", response[i]);
		}
		printf("\n");
	}
}
