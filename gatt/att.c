#include <sys/endian.h>
#include <unistd.h>

#include "att.h"

uint16_t att_exchange_mtu_req(const uint16_t mtu, uint8_t *pdu) {
    if (pdu == NULL)
        return 0;

    pdu[0] = ATT_EXCHANGE_MTU_REQ;
    pdu[1] = htole16(mtu);

    return 3;
}

uint16_t att_read_by_type_req(const uint16_t sh, uint16_t eh, uint16_t type, uint8_t *pdu)
{
    if (pdu == NULL)
        return 0;

    pdu[0] = ATT_READ_BY_TYPE_REQ;
    pdu[1] = htole16(sh);
    pdu[3] = htole16(eh);
    pdu[5] = htole16(type);

    return 7;
}

uint16_t att_read_req(const uint16_t h, uint8_t *pdu) {
    if (pdu == NULL)
        return 0;

    pdu[0] = ATT_READ_REQ;
    pdu[1] = htole16(h);

    return 3;
}

uint16_t att_find_information_req(
        const uint16_t start, const uint16_t end, uint8_t *pdu)
{
        if (pdu == NULL)
            return 0;

        pdu[0] = ATT_FIND_INFORMATION_REQ;
        pdu[1] = htole16(start);
        pdu[3] = htole16(end);

        return 5;
}
