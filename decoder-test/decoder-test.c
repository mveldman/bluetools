#include <att.h>
#include <stdio.h>

void test_dec_mtu_rsp();
void test_dec_find_information_rsp_format_01();

int
main(int argc, char *argv[])
{
	test_dec_mtu_rsp();
	test_dec_find_information_rsp_format_01();
	return (0);
} /* main */

void
test_dec_mtu_rsp()
{
	uint8_t mtu_rsp_pdu[] = {0x03, 0x01, 0x02};

	uint16_t mtu;

 	printf("dec_att_exchange_mtu_rsp\n");
 	mtu =  dec_att_exchange_mtu_rsp(mtu_rsp_pdu);
 	printf("mtu: %04x\n", mtu);
}

void
test_dec_find_information_rsp_format_01()
{
	uint8_t find_information_rsp_pdu[] = {
		0x05, 0x01, 0x1f, 0x00, 0x00,
		0x28, 0x20, 0x00, 0x03, 0x28
	};
	
	att_information_data_list_p	rsp;

 	printf("dec_att_find_information_rsp\n");

	rsp = dec_att_find_information_rsp(find_information_rsp_pdu, 10);
	if (NULL == rsp) {
		printf("Invalid Response\n");
		return;
	}
 	printf("Information format: %02x\n", rsp->format);
 	printf("Information length: %02x\n", rsp->length);
}
