#include <att.h>
#include <stdio.h>

int
main(int argc, char *argv[])
{
	uint16_t len;
	uint16_t start_handle, end_handle;
	uint16_t attribute_type;
	uint8_t	 attribute_value[7] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
	uint16_t	 handles[7] = {0x0001, 0x0002, 0x0300, 0x0400, 0x0506, 0x0607, 0x0708};
	uint16_t offset;

	uint16_t handle;
	uint8_t	 signature[12] = 	{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 
					0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b};

 	printf("enc_att_exchange_mtu_req(%04x)\n", 0x123);
 	enc_att_exchange_mtu_req(0x123, &len);

	start_handle = 0x123;
	end_handle = 0x4567;

 	printf("enc_att_find_information_req(%04x, %04x)\n", start_handle, end_handle);
	enc_att_find_information_req(start_handle, end_handle, &len);

	attribute_type = 0x9876;
 	printf("enc_att_find_by_type_value_req(%04x, %04x, %04x, attribute_value,...)\n",
		start_handle, end_handle, attribute_type);
	enc_att_find_by_type_value_req(start_handle, end_handle, attribute_type,
		attribute_value, sizeof(attribute_value), &len);

 	printf("enc_att_read_by_type_req(%04x, %04x, attribute_type,...)\n",
		start_handle, end_handle);
	enc_att_read_by_type_req(start_handle, end_handle, attribute_value,
		sizeof(attribute_value), &len);

	handle = 0x3456;
 	printf("enc_att_read_req(%04x)\n", handle);
	enc_att_read_req(handle, &len);

	offset = 0x7128;
 	printf("enc_att_read_blob_req(%04x)\n", handle);
	enc_att_read_blob_req(handle, offset, &len);

 	printf("enc_att_read_multiple_req(%04x)\n", handle);
	enc_att_read_multiple_req(handles, 7, &len);

 	printf("enc_att_read_multiple_variable_req()\n");
	enc_att_read_multiple_variable_req(handles, 7, &len);

 	printf("enc_att_write_req(%04x)\n", handle);
	enc_att_write_req(handle, attribute_value, 7, &len);

 	printf("enc_att_write_cmd(%04x)\n", handle);
	enc_att_write_cmd(handle, attribute_value, 7, &len);

 	printf("enc_att_signed_write_cmd(%04x)\n", handle);
	enc_att_signed_write_cmd(handle, attribute_value, 7, signature, &len);

	offset = 0x7895;
 	printf("enc_att_prepare_write_req(%04x, %04x)\n", handle, offset);
	enc_att_prepare_write_req(handle, offset, attribute_value, 7, &len);

 	printf("enc_att_execute_write_req(%04x)\n", 0x01);
	enc_att_execute_write_req(0x01, &len);

 	printf("enc_att_handle_value_ntf(%04x)\n", handle);
	enc_att_handle_value_ntf(handle, attribute_value, 7, &len);

	return (0);
} /* main */
